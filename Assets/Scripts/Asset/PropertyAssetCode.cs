﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropertyAssetCode : AssetCode
{
    [SerializeField] private string Collection;
    [SerializeField] private int HouseNum, standardFee, housePrice, numOfPropInCollection;
    [SerializeField] private bool isCollectionFull = false;
    [SerializeField] private GameManager GameManagerScript;
    [SerializeField] private GameObject[] Houses;


    // Start is called before the first frame update
    void Start()
    {
        GameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void setOwner(int ownerID)
    {
        int counter = 0;    
        base.setOwner(ownerID);
        for (int i = 0; i < GameManagerScript.GetPropertyAssetCodes().Length; i++)
        {
            if (this.Collection.Equals(GameManagerScript.GetPropertyAssetCodes()[i].GetCol()) && GameManagerScript.GetPropertyAssetCodes()[i].GetOwnerID() == ownerID)
            {
                counter++;
            }
        }
        if(counter == numOfPropInCollection)
        {
            isCollectionFull = true;
            for (int i = 0; i < GameManagerScript.GetPropertyAssetCodes().Length; i++)
            {
                if (this.Collection.Equals(GameManagerScript.GetPropertyAssetCodes()[i].GetCol()))
                {
                    GameManagerScript.GetPropertyAssetCodes()[i].SetIsCollectionFull(true);
                }
            }

        }
    }
    public string GetCol()
    {
        return this.Collection;
    }
    public override int calcFee()
    {
        if (isCollectionFull)
        {
            if(HouseNum == 0)
            {
                return standardFee * 2;
            }
        }

        return standardFee + (HouseNum * housePrice);
    }
    public int GetHouseNum()
    {
        return HouseNum;
    }
    public void SetIsCollectionFull(bool value)
    {
        this.isCollectionFull = value;
    }
    public bool GetIsCollectionFull()
    {
        return this.isCollectionFull;
    }
    public void BuyHouse()
    {
        Houses[HouseNum++].SetActive(true);
    }
    public int GetHousePrice()
    {
        return this.housePrice;
    }

    public override int GetMortgagePrice()
    {
        return base.GetMortgagePrice() + HouseNum * (housePrice / 2);
    }
    public override void ResetAsset()
    {
        base.ResetAsset();
        HouseNum = 0;
        for (int i = 0; i < Houses.Length; i++)
        {
            Houses[i].SetActive(false);
        }
    }
}
