﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AssetCode : NodeScript
{

    [SerializeField] protected int price, MortgagePrice;
    [SerializeField] protected bool IsOwned = false;
    [SerializeField] protected int ownerID = -1;
    protected int CollectionID;
    [SerializeField] protected bool IsMortgaged;

    public virtual void setOwner(int ownerID)
    {
        IsOwned = true;
        this.ownerID = ownerID;
    }
    public bool getIsOwned() { return IsOwned; }
    public float GetPrice() { return price; }
    public override bool Equals(object obj)
    {
        return obj is AssetCode code &&
               base.Equals(obj) &&
               name == code.name;
    }
    public int GetOwnerID()
    {
        return ownerID;
    }
    public virtual int calcFee() { return 0; }
    public int getCollectionID()
    {
        return CollectionID;
    }
    public void SetIsMortgaged(bool value)
    {
        this.IsMortgaged = value;
    }
    public bool GetIsMortgaged()
    {
        return this.IsMortgaged;
    }
    public virtual int GetMortgagePrice()
    {
        return MortgagePrice;
    }
    public virtual void ResetAsset()
    {
        ownerID = -1;
        IsMortgaged = false;
    }
}
