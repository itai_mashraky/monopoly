﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class ActionNodeRandomFunction : ActionNode
{

    [SerializeField] protected GameManager GameManagerScript;
    [SerializeField] protected List<Action> PlayerQuests;
    [SerializeField] protected UIManager UIManagerScript;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void TransectMoneyFromPlayerToBank(GameObject source, int amount)
    {
        UIManagerScript.DisplayFunction(1, amount, "");

        if (source.transform.CompareTag("Player"))
        {
            source.GetComponent<PlayerController>().SetMoney(source.GetComponent<PlayerController>().GetMoney() - amount);
        }
    }

    public void TransectMoneyFromBankToPlayer(GameObject source, int amount)
    {
        UIManagerScript.DisplayFunction(0, amount, "");

        if (source.transform.CompareTag("Player"))
        {
            source.GetComponent<PlayerController>().SetMoney(source.GetComponent<PlayerController>().GetMoney() + amount);
        }
    }

    public void General(GameObject user)
    {

    }

    public void MoveToPos(GameObject user, int destIndex, string Dest)
    {
        UIManagerScript.DisplayFunction(2, 0, Dest);
        if (user.transform.CompareTag("Player"))
        {
            print("found player");
            user.GetComponent<PlayerMovement_Monoply>().SetPos(destIndex);
        }
    }
}
