﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{

    [SerializeField] private GameObject BuyButton, temp1, MortgageButton, PayMortgageButton, BuyHouseButton, NoMoneyForEndTurnText, EndGamePerPlayerButton, WinMenu;
    [SerializeField] private GameManager GameManagerScript;
    [SerializeField] private GameObject AssetDisplayPro, assetDisplayAir, FunctionDisplay, ActionDisplay, RollButton, EndTurnButton, TurnText, noMoneyText, CubeResultText;
    [SerializeField] private Dropdown OwnedAssetDisplay;


    // Start is called before the first frame update
    void Start()
    {
        GameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();
        OwnedAssetDisplay.onValueChanged.AddListener(delegate
        {
            DisplayDropDownAsset();
        });

    }

    // Update is called once per frame
    void Update()
    {
    }

    public void DisplayProperty(PropertyAssetCode assetCode)
    {
        AssetDisplayPro.SetActive(true);

        Text temp;
        temp1 = GameObject.Find("Name_Text_Pro");
        temp = temp1.GetComponent<Text>();
        temp.text = "Name: " + assetCode.GetName();
        temp = GameObject.Find("Price_Text_Pro").GetComponent<Text>();
        temp.text = "Price: " + assetCode.GetPrice();
        temp = GameObject.Find("Collection_Text_Pro").GetComponent<Text>();
        temp.text = "Collection: " + GameManagerScript.getCollectionByID(assetCode.getCollectionID());
        temp = GameObject.Find("Owned_Text_Pro").GetComponent<Text>();
        temp.text = "Is Owned: " + assetCode.getIsOwned();
        if (assetCode.getIsOwned())
        {
            temp = GameObject.Find("Owner_Name_Text_Pro").GetComponent<Text>();
            temp.text = "Owner name: " + GameManagerScript.GetPlayerNameByID(assetCode.GetOwnerID());
            temp = GameObject.Find("Houes_Num_Text_Pro").GetComponent<Text>();
            temp.text = "house num: " + assetCode.GetHouseNum();
        }
        else
        {
            temp = GameObject.Find("Owner_Name_Text_Pro").GetComponent<Text>();
            temp.text = "";
            temp = GameObject.Find("Houes_Num_Text_Pro").GetComponent<Text>();
            temp.text = "";

        }
    }
    public void DisplayAirPort(AirPortAssetCode assetCode)
    {
        assetDisplayAir.SetActive(true);
        Text temp;
        temp = GameObject.Find("Name_Text_Pro").GetComponent<Text>();
        temp.text = "Name: " + assetCode.GetName();
        temp = GameObject.Find("Price_Text_Pro").GetComponent<Text>();
        temp.text = "Price: " + assetCode.GetPrice();
        temp = GameObject.Find("Collection_Text_Pro").GetComponent<Text>();
        temp.text = "Collection: " + GameManagerScript.getCollectionByID(assetCode.getCollectionID());
        temp = GameObject.Find("Owned_Text_Pro").GetComponent<Text>();
        temp.text = "Is Owned: " + assetCode.getIsOwned();
        if (assetCode.getIsOwned())
        {
            temp = GameObject.Find("Owner_Name_Text_Pro").GetComponent<Text>();
            temp.text = "Owner name: " + GameManagerScript.GetPlayerNameByID(assetCode.GetOwnerID());
        }
        else
        {
            temp = GameObject.Find("Owner_Name_Text_Pro").GetComponent<Text>();
            temp.text = "";
        }
    }
    public void DisplayAgency(CompanyAssetCode assetCode)
    {
        assetDisplayAir.SetActive(true);
        Text temp;
        temp = GameObject.Find("Name_Text_Pro").GetComponent<Text>();
        temp.text = "Name: " + assetCode.GetName();
        temp = GameObject.Find("Price_Text_Pro").GetComponent<Text>();
        temp.text = "Price: " + assetCode.GetPrice();
        temp = GameObject.Find("Collection_Text_Pro").GetComponent<Text>();
        temp.text = "Collection: " + GameManagerScript.getCollectionByID(assetCode.getCollectionID());
        temp = GameObject.Find("Owned_Text_Pro").GetComponent<Text>();
        temp.text = "Is Owned: " + assetCode.getIsOwned();
        if (assetCode.getIsOwned())
        {
            temp = GameObject.Find("Owner_Name_Text_Pro").GetComponent<Text>();
            temp.text = "Owner name: " + GameManagerScript.GetPlayerNameByID(assetCode.GetOwnerID());
        }
        else
        {
            temp = GameObject.Find("Owner_Name_Text_Pro").GetComponent<Text>();
            temp.text = "";
        }
    }
    public void DisplayFunction(int type, int amount, string dest)
    {
        Text temp;
        FunctionDisplay.SetActive(true);
        switch (type)
        {
            case 0:
                temp = GameObject.Find("Action_Text").GetComponent<Text>();
                temp.text = "Action: Pay money";
                GameObject.Find("Dest_Text").GetComponent<Text>().text = "";
                temp = GameObject.Find("Price_Text").GetComponent<Text>();
                temp.text = "Amount: " + amount;
                break;
            case 1:
                temp = GameObject.Find("Action_Text").GetComponent<Text>();
                temp.text = "Action: Recieve money";
                GameObject.Find("Dest_Text").GetComponent<Text>().text = "";
                temp = GameObject.Find("Price_Text").GetComponent<Text>();
                temp.text = "Amount: " + amount;
                break;
            case 2:
                temp = GameObject.Find("Action_Text").GetComponent<Text>();
                temp.text = "Action: move to";
                GameObject.Find("Price_Text").GetComponent<Text>().text = "";
                temp = GameObject.Find("Dest_Text").GetComponent<Text>();
                temp.text = dest;

                break;
        }
    }
    public void DisplayAction(string action)
    {
        ActionDisplay.SetActive(true);
        GameObject.Find("Action_Text").GetComponent<Text>().text = action;

    }
    public void EnablePurchase()
    {
        BuyButton.SetActive(true);
    }
    public void buy()
    {
        ResetUI();
        GameManagerScript.GetActivePlayerController().BuyAsset();
        AssetCode item = GameManagerScript.GetActivePlayerController().GetActiveAsset();
        if (item.gameObject.CompareTag("Property"))
        {
            DisplayProperty((PropertyAssetCode)item);
        }
        else if (item.gameObject.CompareTag("AirPort"))
        {
            DisplayAirPort((AirPortAssetCode)item);
        }
        else
        {
            DisplayAgency((CompanyAssetCode)item);
        }

    }
    public void SetRollButton(bool value)
    {
        RollButton.SetActive(value);
    }
    public void SetEndTurnButton(bool value)
    {
        EndTurnButton.SetActive(value);
    }
    public void SetTurnText(string text)
    {
        TurnText.GetComponent<Text>().text = text;
        BuyHouseButton.SetActive(false); // cant turn off button in Reset_UI because of DropDownMenu
    }
    public void NoMoney()
    {
        noMoneyText.SetActive(true);
    }
    public void SetCubeResultText(int a, int b)
    {
        CubeResultText.GetComponent<Text>().text = a + " , " + b;
    }
    public void ResetUI()
    {
        BuyButton.SetActive(false);
        assetDisplayAir.SetActive(false);
        AssetDisplayPro.SetActive(false);
        FunctionDisplay.SetActive(false);
        ActionDisplay.SetActive(false);
        noMoneyText.SetActive(false);
        OwnedAssetDisplay.gameObject.SetActive(false);
        MortgageButton.SetActive(false);
        PayMortgageButton.SetActive(false);
    }
    public void PopulateDropdown(Dropdown dropdown, List<AssetCode> OptionsList)
    {
        string Name;
        PropertyAssetCode property;
        List<string> options = new List<string>();
        foreach (var option in OptionsList)
        {
            if (option.gameObject.CompareTag("Property"))
            {
                property = (PropertyAssetCode)option;
                Name = option.GetName() + ", Price: " + property.GetMortgagePrice();
            }
            else
            {
                Name = option.GetName() + ", Price: " + option.GetMortgagePrice();
            }

            options.Add(Name);
        }
        dropdown.ClearOptions();
        dropdown.AddOptions(options);
    }
    public void FillOwnedAssetDisplay(List<AssetCode> assetCodes)
    {
        string Name;
        PropertyAssetCode property;

        OwnedAssetDisplay.gameObject.SetActive(true);
        PopulateDropdown(OwnedAssetDisplay, assetCodes);
        foreach (AssetCode item in GameManagerScript.GetActivePlayerController().GetOwnedAssetCodes())
        {
            if (item.gameObject.CompareTag("Property"))
            {
                property = (PropertyAssetCode)item;
                Name = item.GetName() + ", Price: " + property.GetMortgagePrice();
            }
            else
            {
                Name = item.GetName() + ", Price: " + item.GetMortgagePrice();
            }

            if (Name.Equals(OwnedAssetDisplay.options[OwnedAssetDisplay.value].text))
            {
                if (item.GetIsMortgaged())
                {
                    if (GameManagerScript.GetActivePlayerController().GetMoney() >= item.GetMortgagePrice())
                    {
                        PayMortgageButton.SetActive(true);
                    }
                }
                else
                {
                    MortgageButton.SetActive(true);
                }
                break;

            }
        }
    }
    public void DisplayDropDownAsset()
    {

        OwnedAssetDisplay.gameObject.SetActive(true);
        foreach (AssetCode item in GameManagerScript.GetActivePlayerController().GetOwnedAssetCodes())
        {
            if (item.GetName().Equals(OwnedAssetDisplay.options[OwnedAssetDisplay.value].text))
            {
                if (item.gameObject.CompareTag("Property"))
                {
                    DisplayProperty((PropertyAssetCode)item);
                }
                else if (item.gameObject.CompareTag("AirPort"))
                {
                    DisplayAirPort((AirPortAssetCode)item);
                }
                else
                {
                    DisplayAgency((CompanyAssetCode)item);
                }

                if (item.GetIsMortgaged())
                {
                    if (GameManagerScript.GetActivePlayerController().GetMoney() >= item.GetMortgagePrice())
                    {
                        PayMortgageButton.SetActive(true);
                    }
                }
                else
                {
                    MortgageButton.SetActive(true);
                }
                break;


            }
        }
    }
    public void PayMortgage()
    {
        OwnedAssetDisplay.gameObject.SetActive(true);
        foreach (AssetCode item in GameManagerScript.GetActivePlayerController().GetOwnedAssetCodes())
        {
            if (item.GetName().Equals(OwnedAssetDisplay.options[OwnedAssetDisplay.value].text))
            {
                item.SetIsMortgaged(false);
                GameManagerScript.GetActivePlayerController().SetMoney(GameManagerScript.GetActivePlayerController().GetMoney() - item.GetMortgagePrice());

                if (item.gameObject.CompareTag("Property"))
                {
                    DisplayProperty((PropertyAssetCode)item);
                }
                else if (item.gameObject.CompareTag("AirPort"))
                {
                    DisplayAirPort((AirPortAssetCode)item);
                }
                else
                {
                    DisplayAgency((CompanyAssetCode)item);
                }
            }
        }
        MortgageButton.SetActive(true);
    }
    public void MortgageAsset()
    {
        OwnedAssetDisplay.gameObject.SetActive(true);
        foreach (AssetCode item in GameManagerScript.GetActivePlayerController().GetOwnedAssetCodes())
        {
            if (item.GetName().Equals(OwnedAssetDisplay.options[OwnedAssetDisplay.value].text))
            {
                item.SetIsMortgaged(true);
                GameManagerScript.GetActivePlayerController().SetMoney(GameManagerScript.GetActivePlayerController().GetMoney() + item.GetMortgagePrice());

                if (item.gameObject.CompareTag("Property"))
                {
                    DisplayProperty((PropertyAssetCode)item);
                }
                else if (item.gameObject.CompareTag("AirPort"))
                {
                    DisplayAirPort((AirPortAssetCode)item);
                }
                else
                {
                    DisplayAgency((CompanyAssetCode)item);
                }
            }
        }
        PayMortgageButton.SetActive(true);
        GameManagerScript.GetActivePlayerController().CheckEndTurn();

    }
    public void SetBuyHouseButton(bool value)
    {
        this.BuyHouseButton.SetActive(value);
    }
    public void BuyHouse()
    {
        PropertyAssetCode activeProperty = (PropertyAssetCode)GameManagerScript.GetActivePlayerController().GetActiveAsset();
        if (activeProperty.GetHouseNum() >= 4 || GameManagerScript.GetActivePlayerController().GetMoney() < activeProperty.GetHousePrice())
        {
            BuyHouseButton.SetActive(false);
        }
        GameManagerScript.GetActivePlayerController().BuyHouse();
        DisplayProperty(activeProperty);

    }
    public void SetNoMoneyForEndText(bool value)
    {
        this.NoMoneyForEndTurnText.SetActive(value);
    }
    public void SetEndGamePerPlayerButton(bool value)
    {
        this.EndGamePerPlayerButton.SetActive(value);
    }
    public void EndGamePerPlayer()
    {
        GameManagerScript.GetActivePlayerController().EndGame();
    }
    
    public void OpenWinMenu()
    {
        WinMenu.SetActive(true);
        GameObject.Find("WinnerText").GetComponent<Text>().text = "Winner: " + GameManagerScript.GetActivePlayerController().GetName();
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }
}
